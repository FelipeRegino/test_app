# imagem de origem
FROM node:14.14.0-alpine

# diretório de trabalho (é onde a aplicação ficará dentro do container).
WORKDIR /app

# copiando arquivos para o container
COPY . .

# adicionando `/app/node_modules/.bin` para o $PATH
ENV PATH /app/node_modules/.bin:$PATH

# instalando bash
RUN apk add --no-cache bash

# instalando dependências da aplicação e armazenando em cache.
RUN npm install npm@latest -g
RUN npm install --silent
RUN npm install react-scripts@3.4.3 -g --silent
RUN npm install -g expo-cli
RUN yarn add expo